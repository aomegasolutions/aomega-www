#!/bin/sh

if [ ! -f ~/.aws/credentials ]; then
  mkdir -p ~/.aws

  printf "%s\n" "[${INFRA_PROFILE}]" "aws_access_key_id=${INFRA_ACCESS_KEY_ID}" "aws_secret_access_key=${INFRA_SECRET_ACCESS_KEY}" "[${STAGE_PROFILE}]" "aws_access_key_id=${STAGE_ACCESS_KEY_ID}" "aws_secret_access_key=${STAGE_SECRET_ACCESS_KEY}" "[${PROD_PROFILE}]" "aws_access_key_id=${PROD_ACCESS_KEY_ID}" "aws_secret_access_key=${PROD_SECRET_ACCESS_KEY}" > ~/.aws/credentials
else
  echo "AWS credentials file already exists"
  cat ~/.aws/credentials
fi

if [ ! -f ~/.aws/config ]; then
  mkdir -p ~/.aws

  printf "%s\n" "[profile ${INFRA_PROFILE}]" "region=${INFRA_REGION}" "[profile ${STAGE_PROFILE}]" "region=${STAGE_REGION}" "[profile ${PROD_PROFILE}]" "region=${PROD_REGION}" > ~/.aws/config
else
  echo "AWS config file already exists"
  cat ~/.aws/config
fi

