import React from "react";
import styled from "@emotion/styled";
import "./css/main.css";
import logo from "./assets/watchmen.png";

const UnderConstruction = styled.div`
  height: 100vh;
  width: 100vw;
  background-repeat: no-repeat;
  background-image: url(${logo});
  background-position: center;
  background-size: 50%;
`;

const Copyright = styled.footer`
  font-family: Helvetica, Arial, sans-serif;
  font-size: 1.3vh;
  position: absolute;
  bottom: 20px;
  width: 100vw;
  text-align: center;
  color: #f6d26e;
`;
export const App = () => (
  <>
    <UnderConstruction />
    <Copyright>© 2020 Alpha Omega Web Solutions</Copyright>
  </>
);
