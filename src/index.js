import React from "react";
import ReactDOM from "react-dom";
import "normalize.css";
import { App } from "./app";

console.log(App);
ReactDOM.render(<App />, document.getElementById("app"));
