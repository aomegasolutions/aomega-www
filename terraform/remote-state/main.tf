terraform {
  backend "s3" {
    bucket = "aomega-terraform-remote-state"
    dynamodb_table = "terraform-state-lock-dynamo"
    key     = "aomega-stage/global/terraform.state"
    region  = "eu-west-1"
    encrypt = true
    profile = "aomega_stage"
  }
}
