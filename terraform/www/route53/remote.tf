data "terraform_remote_state" "cloudfront_distribution" {
  backend = "s3"
  config = {
    region  = var.aws_region
    bucket  = "${var.aws_account_id}-aomega-terraform-remote-state"
    key     = "${var.environment}/www/cloudfront"
    profile = var.aws_profile
  }
}

data "terraform_remote_state" "zone_id" {
  backend = "s3"
  config = {
    region  = "eu-west-1"
    bucket  = "147997506779-aomega-terraform-remote-state"
    key     = "aomega-infra/global/hosted-zone/terraform.state"
    profile = "aomega_infra"
  }
}
