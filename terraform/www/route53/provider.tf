provider "aws" {
  profile = var.aws_profile               
  region = var.aws_region                  
  allowed_account_ids = [var.aws_account_id, "147997506779"]
  assume_role {
    role_arn = "arn:aws:iam::147997506779:role/aomega-cross-account-infra"
  }
  version = "2.41"
}

provider "template" {                     
  version = "2.1.2"
} 

provider "local" {                         
  version = "1.4"
}

provider "null" {                            
  version = "2.1.2"
} 

provider "tls" {                            
  version = "2.1.1"
}
