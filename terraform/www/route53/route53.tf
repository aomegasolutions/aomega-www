resource "aws_route53_record" "www" {
  zone_id = data.terraform_remote_state.zone_id.outputs.dns.aomegasolutions_com
  name = var.domain
  type = "A"

  alias {
    name = data.terraform_remote_state.cloudfront_distribution.outputs.cloudfront_website_domain.www_aomegasolutions_com
    zone_id = data.terraform_remote_state.cloudfront_distribution.outputs.cloudfront_hosted_zone_id.www_aomegasolutions_com
    evaluate_target_health = false
  }
}
