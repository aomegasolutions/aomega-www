variable "aws_region" {
  description = "AWS region"
  type        = string
  default     = "eu-west-1"
}
variable "environment" {
  description = "A name that identifies the environment, will used as prefix and for tagging."
  type        = string
  default     = "aomega-stage"
}
variable "certificate_domain" {}
variable "aws_profile" {}
variable "aws_account_id" {}
variable "secret" {
  type = string
  default = ""
}
