output "lambda_arn" {
  value = {
    stage_aomegasolutions_com = "${module.authentication_lambda.arn}:${module.authentication_lambda.version}"
  }
}

