module "authentication_lambda" {
  source = "github.com/riboseinc/terraform-aws-lambda-edge-authentication"

  /* S3 bucket that stores configuration JSON file. */
  bucketName = "${aws_s3_bucket.permissions[0].bucket}"

  /* S3 object name of the configuration JSON file in the above bucket. */
  bucketKey = "${aws_s3_bucket_object.permissions[0].key}"

  /* the domain scope of cookie to be set */
  cookieDomain = var.certificate_domain

  providers = {
    aws = aws.cloudfront
  }
}
