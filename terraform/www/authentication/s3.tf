provider "aws" {
  region = "us-east-1"
  profile = var.aws_profile
  # description = "AWS Region for Cloudfront (ACM certs only supports us-east-1)"
  alias = "cloudfront"
}

resource "aws_s3_bucket" "permissions" {
  count = var.environment == "aomega-stage" ? 1 : 0
  bucket = "aomega-staging-permissions"
  acl    = "private"
  provider = aws.cloudfront
}

resource "aws_s3_bucket_object" "permissions" {
  count = var.environment == "aomega-stage" ? 1 : 0
  bucket = aws_s3_bucket.permissions[0].bucket
  key    = "config.json"

  # Assume that your configuration JSON file is stored locally at `config.json`
  source = "./config.json"
  etag = filemd5("./config.json")

  provider = aws.cloudfront
}
