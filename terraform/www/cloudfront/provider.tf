provider "aws" {
  profile             = var.aws_profile
  region              = var.aws_region
  allowed_account_ids = [var.aws_account_id, "147997506779"]
  version = "2.41"
}

provider "template" {
  version = "2.1.2"
}

provider "local" {
  version = "1.4"
}

provider "null" {
  version = "2.1.2"
}

provider "tls" {
  version = "2.1.1"
}

provider "aws" {
  alias               = "use1"
  region              = "us-east-1"
  profile             = var.aws_profile
  allowed_account_ids = [var.aws_account_id]
}
