resource "aws_acm_certificate" "site_certificate" {
  provider    = aws.use1
  domain_name = "*.${var.certificate_domain}"
  subject_alternative_names = var.subject_alternative_domains

  validation_method = "EMAIL"
}

resource "aws_acm_certificate_validation" "site_certificate" {
  provider        = aws.use1
  certificate_arn = aws_acm_certificate.site_certificate.arn
}
