output "s3_website_domain" {
  value = {
    www_aomegasolutions_com = "${aws_s3_bucket.cloudfront_bucket.website_domain}"
  }
}

output "s3_hosted_zone_id" {
  value = {
    www_aomegasolutions_com = "${aws_s3_bucket.cloudfront_bucket.hosted_zone_id}"
  }
}

output "cloudfront_website_domain" {
  value = {
    www_aomegasolutions_com = "${aws_cloudfront_distribution.www_aomegasolutions_com.domain_name}"
  }
}

output "cloudfront_hosted_zone_id" {
  value = {
    www_aomegasolutions_com = "${aws_cloudfront_distribution.www_aomegasolutions_com.hosted_zone_id}"
  }
}
