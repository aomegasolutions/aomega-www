resource "aws_cloudfront_distribution" "www_aomegasolutions_com" {
  provider        = aws.use1
  enabled         = true
  http_version    = "http2"
  is_ipv6_enabled = true

  origin {
    domain_name = aws_s3_bucket.cloudfront_bucket.bucket_domain_name
    origin_id   = var.site_bucket

    # Only run in stage - where authenticate is populated
    dynamic "custom_header" {
      for_each = var.authenticate
      content {
        name  = "User-Agent"
        value = base64sha512("${var.certificate_domain}-${var.secret}")
      }
    }
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  default_root_object = "index.html"

  default_cache_behavior {
    target_origin_id = var.site_bucket

    allowed_methods = ["GET", "HEAD"]
    cached_methods  = ["GET", "HEAD"]

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 7200
    max_ttl                = 86400

    # Only run in stage - where authenticate is populated
    dynamic "lambda_function_association" {
      for_each = var.authenticate
      # Link the Lambda function to CloudFront request
      # for authenticating
      content {
        event_type = "viewer-request"
        lambda_arn = data.terraform_remote_state.authentication[0].outputs.lambda_arn.stage_aomegasolutions_com
      }
    }

    # Only run in stage - where authenticate is populated
    dynamic "lambda_function_association" {
      for_each = var.authenticate
      # Link the Lambda function to CloudFront response
      # for setting the authenticated cookie
      content {
        event_type = "viewer-response"
        lambda_arn = data.terraform_remote_state.authentication[0].outputs.lambda_arn.stage_aomegasolutions_com
      }
    }
  }

  // Here we're ensuring we can hit this distribution using www.runatlantis.io
  // rather than the domain name CloudFront gives us.
  aliases = var.cloudfront_aliases

  viewer_certificate {
    acm_certificate_arn      = aws_acm_certificate_validation.site_certificate.certificate_arn
    minimum_protocol_version = "TLSv1.2_2018"
    ssl_support_method       = "sni-only"
  }
}

