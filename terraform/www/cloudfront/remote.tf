data "terraform_remote_state" "hosted_zone" {
  backend = "s3"
  config = {
    region  = "eu-west-1"
    bucket  = "147997506779-aomega-terraform-remote-state"
    key     = "aomega-infra/global/hosted-zone/terraform.state"
    profile = "aomega_infra"
  }
}

data "terraform_remote_state" "authentication" {
  count = var.environment == "aomega-stage" ? 1 : 0
  backend = "s3"
  config = {
    region  = var.aws_region
    bucket  = var.remote_state_bucket
    key     = var.authentication_state_key
    profile = var.aws_profile
  }
}
