variable "aws_region" {
  description = "AWS region"
  type        = string
  default     = "eu-west-1"
}

variable "environment" {
  description = "A name that identifies the environment, will used as prefix and for tagging."
  type        = string
  default     = "aomega-stage"
}

variable "domain_name" {}
variable "subdomain" {}
variable "certificate_domain" {}
variable "site_bucket" {}
variable "remote_state_bucket" {}
variable "authentication_state_key" {}
variable "aws_profile" {}
variable "aws_account_id" {}
variable "cloudfront_aliases" {}
variable "subject_alternative_domains" {}
variable "authenticate" {
  type    = set(number)
  default = []
}
variable "secret" {
  type    = string
  default = "supersecret"
}
