resource "aws_s3_bucket" "cloudfront_bucket" {
  bucket = "${var.subdomain}.${var.domain_name}"
  acl    = "public-read"
  force_destroy = true
  policy = <<POLICY
{
  "Version":"2012-10-17",
  "Statement":[{
    "Sid":"PublicReadForGetBucketObjects",
        "Effect":"Allow",
      "Principal": "*",
      "Action":"s3:GetObject",
      "Resource":["arn:aws:s3:::${var.subdomain}.${var.domain_name}/*"
      ]
    }
  ]
}
POLICY

  website {
    index_document = "index.html"
  }
}
